package repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import model.Article;
import model.Avis;
@Repository
public interface AvisRepository extends JpaRepository<Avis, Long> {

	List<Avis> findAllByArticle(Article article);
}

