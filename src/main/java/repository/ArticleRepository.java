package repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import model.Article;
@Repository
public interface ArticleRepository extends JpaRepository<Article, Long> {
	
	@Query(value = "SELECT avg(note) from Avis where articleId = ?1", nativeQuery = true)
	   int getMoyenne(long articleId);
}

