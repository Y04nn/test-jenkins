package com.example.demo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import model.Article;
import model.ArticleByMoyenneComparator;
import repository.ArticleRepository;
import vue.ArticleDisplay;

@RestController
@Configuration
public class ArticleController {
	@Autowired 
	ArticleRepository articleRepository;



	//Tester avec Postman Body x-www-form-urlencoded avec parametre descrpition et prix => localhost:8080/article
	@RequestMapping(value= "/article" ,method=RequestMethod.POST)
	public ResponseEntity creerArticle(@ModelAttribute("article") Article article) throws IOException {

		articleRepository.save(article);
		return new ResponseEntity(null, HttpStatus.CREATED);
	}

	//Tester avec Postman localhost:8080/article/1
	@RequestMapping(value= "/article/{id}" ,method=RequestMethod.GET)
	public ResponseEntity afficherUnArticle(@PathVariable("id")long id) {
		Article findById = articleRepository.findById(id).get();
		ArticleDisplay articleCherche= new ArticleDisplay(findById);

		if(articleCherche!=null) {
			return new ResponseEntity(articleCherche, HttpStatus.OK);
		}
		else {
			return new ResponseEntity(articleCherche, HttpStatus.NOT_FOUND);
		}
	}
	//Tester avec localhost:8080/articles?sortBy=id&direction=DES
	@GetMapping(value= "/articles")
	public ResponseEntity afficherListeArticle(
			@RequestParam(value = "sortBy", required = false, defaultValue = "price") String sortBy,
			@RequestParam(value = "order", required = false, defaultValue = "ASC") String order) {


		List<ArticleDisplay> listeTrier=new ArrayList<ArticleDisplay>();


		if(sortBy.equals("id") || sortBy.equals("description") || sortBy.equals("price")) {
			List<Article> findAll = articleRepository.findAll(new Sort(Sort.Direction.fromString(order), sortBy));
			for(Article article : findAll) {

				ArticleDisplay articleDisplay= new ArticleDisplay(article);
				
				listeTrier.add(articleDisplay);
			}
		}
		else if(sortBy=="moyenne") {
			List<Article> findAll = articleRepository.findAll();
			for(Article article : findAll) {
				ArticleDisplay articleDisplay= new ArticleDisplay(article);
				listeTrier.add(articleDisplay);
				Collections.sort(listeTrier, new ArticleByMoyenneComparator());
			}
		}
		return new ResponseEntity(listeTrier, HttpStatus.OK);
	}

	//Tester avec Postman en Delete localhost:8080/article/1
	@RequestMapping(value= "/article/{id}" ,method=RequestMethod.DELETE)
	public ResponseEntity deleteArticle(@PathVariable("id")long id) {
		try {
			Article findById = articleRepository.findById(id).get();
			articleRepository.delete(findById);
			return new ResponseEntity(null, HttpStatus.OK);
		}catch (IllegalArgumentException e) {
			return new ResponseEntity("Article n'existe plus", HttpStatus.NOT_FOUND);
		}

	}

}
