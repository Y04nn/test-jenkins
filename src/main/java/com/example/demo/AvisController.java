package com.example.demo;

import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import model.Article;
import model.Avis;
import model.Message;
import repository.ArticleRepository;
import repository.AvisRepository;
import vue.AvisDisplay;


@RestController
@Configuration
public class AvisController {
	@Autowired 
	AvisRepository avisRepository;
	@Autowired 
	ArticleRepository articleRepository;

	public static long idAvis= 00001;
	//Tester avec Postman Body x-www-form-urlencoded avec parametre descrpition et prix => localhost:8080/article/2/advice
	@PostMapping(value= "/article/{id}/advice")
	public ResponseEntity creerAvis(@ModelAttribute("advice") Avis advice , @PathVariable("id")long id) throws IOException {
		Article findById = articleRepository.findById(id).get();
		
			if(advice.getNote()>=0 && advice.getNote()<=5) {
				Date date=new Date();
				advice.setDate(date);
				advice.setArticle(findById);
				avisRepository.save(advice);
			}
		
		Message message = new Message();
		message.setDescription("Article créé");
		
		return new ResponseEntity(message, HttpStatus.CREATED);
	}
	
	//Tester avec Postman localhost:8080/article/2/advices?commentaire=false et/ou true
	@GetMapping(value= "/article/{id}/advices" )
	public ResponseEntity afficherListAvisDunArticle(@PathVariable("id")long id, @RequestParam(value = "commentaire", required = false, defaultValue = "true")boolean commentaire) {
		
		Article article = articleRepository.getOne(id);
		List<Avis> findAllByArticle = avisRepository.findAllByArticle(article);


		if(articleRepository.getOne(id)!=null) {
			if(!commentaire) {
				return new ResponseEntity(findAllByArticle, HttpStatus.OK);
			}
			else {

				Set<AvisDisplay> listAvisAvecCommentaire = new HashSet<>();
				for (Avis avisEntitee : findAllByArticle) {
					
					if(avisEntitee.getCommentaire()!=null) {
						AvisDisplay avisDisplay = new AvisDisplay(avisEntitee);
						listAvisAvecCommentaire.add(avisDisplay);
					}
				}
				return new ResponseEntity(listAvisAvecCommentaire, HttpStatus.OK);
			}
		}
		else {
			return new ResponseEntity(null, HttpStatus.NOT_FOUND);
		}
	}
//	//Tester avec Postman en Get localhost:8080/advice/3
//	@GetMapping(value= "/advice/{id}")
//	public ResponseEntity afficherAvis(@PathVariable("id")long id) throws IOException {
//
//		for (Article article : ArticleController.listArticle) {
//			for(Avis avis : article.getListAvis()) {
//				if(avis.getIdAvis()==id) {
//					return new ResponseEntity(avis, HttpStatus.OK);
//				}
//			}
//		}
//		return new ResponseEntity(null, HttpStatus.NOT_FOUND);
//	}
//	//Tester avec Postman en Delete localhost:8080/advice/3	
//	@DeleteMapping(value= "/advice/{id}")
//	public ResponseEntity deleteAvis(@PathVariable("id")long id) throws IOException {
//
//		for (Article article : ArticleController.listArticle) {
//			for(Avis avis : article.getListAvis()) {
//				if(avis.getIdAvis()==id) {
//					article.getListAvis().remove(avis);
//					return new ResponseEntity(null, HttpStatus.OK);
//				}
//			}
//		}
//		return new ResponseEntity(null, HttpStatus.NOT_FOUND);
//	}
	
}
