package model;

import java.util.Comparator;

import vue.ArticleDisplay;

public class ArticleByPriceComparator implements Comparator<ArticleDisplay> {

    public int compare(ArticleDisplay a1, ArticleDisplay a2) {
       return (int) (a1.getPrix()- a2.getPrix());
    }
}
