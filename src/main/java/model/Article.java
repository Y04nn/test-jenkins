package model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
@Entity
public class Article {
	@Id
	@GeneratedValue
	protected long id;
	protected  String description;
	protected  double prix;
	@OneToMany(mappedBy="article")
	private List<Avis> listAvis =new ArrayList<Avis>();

	

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getPrix() {
		return prix;
	}
	public void setPrix(double prix) {
		this.prix = prix;
	}
	public List<Avis> getListAvis() {
		return listAvis;
	}
	public void setListAvis(List<Avis> listAvis) {
		this.listAvis = listAvis;
	}

	



		

	
	

}
