package model;

import java.util.Comparator;

import vue.ArticleDisplay;

public class ArticleByDescriptionComparator implements Comparator<ArticleDisplay> {

    public int compare(ArticleDisplay a1, ArticleDisplay a2) {
       return a1.getDescription().compareTo(a2.getDescription());
    }
}
