package model;

import java.util.Comparator;

import vue.ArticleDisplay;

public class ArticleByMoyenneComparator implements Comparator<ArticleDisplay> {

	
    public int compare(ArticleDisplay a1, ArticleDisplay a2) {
        return (int) (a1.getMoyenne()- a2.getMoyenne());
     }



}
