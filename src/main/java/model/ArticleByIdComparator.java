package model;

import java.util.Comparator;

import vue.ArticleDisplay;

public class ArticleByIdComparator implements Comparator<ArticleDisplay> {

    public int compare(ArticleDisplay a1, ArticleDisplay a2) {
       return (int) (a1.getId()- a2.getId());
    }
}
