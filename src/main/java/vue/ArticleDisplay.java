package vue;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnore;

import model.Article;
import model.Avis;

public class ArticleDisplay{
		
	public ArticleDisplay(Article article) {
		super();
		this.id = article.getId();
		this.description = article.getDescription();
		this.prix = article.getPrix();
		this.listAvis = article.getListAvis();
		this.moyenne = this.getMoyenne();
	}
	protected long id;
	protected  String description;
	protected  double prix;
	@JsonIgnore
	private List<Avis> listAvis =new ArrayList<Avis>();
	double moyenne;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getPrix() {
		return prix;
	}
	public void setPrix(double prix) {
		this.prix = prix;
	}
	public List<Avis> getListAvis() {
		return listAvis;
	}
	public void setListAvis(List<Avis> listAvis) {
		this.listAvis = listAvis;
	}
	public double getMoyenne() {
		double noteTotale=0;
		double nbreNote=0;
		List<Avis> listAvisArticle = this.getListAvis();
		for(Avis avis : listAvisArticle) {
			noteTotale += avis.getNote();
			nbreNote++;
		}
		this.moyenne=noteTotale/nbreNote;
		return noteTotale/nbreNote;
	}
	public void setMoyenne(double moyenne) {
		this.moyenne = moyenne;
	}

	

}
