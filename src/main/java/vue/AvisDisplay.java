package vue;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import model.Article;
import model.Avis;

public class AvisDisplay {

	private long idAvis;
	private String commentaire;
	private int note;
	Date date;

	public AvisDisplay(Avis avis) {
		super();
		this.idAvis = avis.getIdAvis();
		this.commentaire = avis.getCommentaire();
		this.note = avis.getNote();
		this.date = avis.getDate();
	}
	public long getIdAvis() {
		return idAvis;
	}
	public void setIdAvis(long idAvis) {
		this.idAvis = idAvis;
	}
	public String getCommentaire() {
		return commentaire;
	}
	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	public int getNote() {
		return note;
	}
	public void setNote(int note) {
		this.note = note;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}


	



		

	
	

}
